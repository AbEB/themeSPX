SPX Custom Theme
=============

![screenshot](https://gitlab.com/AbEB/themeSPX/raw/master/screenshot.png?inline=false)

Forked from FlatSIS - François Jacquet : https://gitlab.com/francoisjacquet/FlatSIS

Version: August 2024

# Description

This theme is a customized adaptation of the FlatSIS theme, specifically designed to meet the needs of SPX schools. It retains the flat design while introducing adjustments to enhance user experience and align with the specific requirements of SPX schools.

# Modifications Made

- **SPX Customization**: Adapted the theme to better fit the specific needs of SPX schools, including layout adjustments and custom features.
- **Color Palette**: Updated the color scheme to reflect the visual identity of SPX schools, based on EDM theme.
- **Icons**: No change for the moment
- **Typography**: No change for the moment
- **Layout**: Customisation of interface layout

# Content

## Buttons

Button icons are located in the `btn/` folder.

## Module Icons

Module icons are located in the `modules/` folder.

License: see icon folders for their respective license. - Author François Jacquet

## Logo

To customize the logo, replace the `logo.png` file with your own logo.

## Font

Like FlatSIS, this theme uses the _Lato_ font instead of default the _Open Sans_.

## Stylesheet

The `stylesheet.css` file links to the base theme's stylesheet and redefines module icons. Feel free to add your custom styles or redefine the base theme’s styles in this stylesheet.

# Installation

1. Copy the `SPXTheme/` folder (rename it if named differently) and its contents into the `assets/themes/` folder of RosarioSIS.
2. Go to _School Setup > School Configuration_ and select it as the **Default Theme**.
3. Users can choose their preferred theme by going to _Users > My Preferences_.

**Requires RosarioSIS 4.4+**